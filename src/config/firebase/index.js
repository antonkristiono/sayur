import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";


const firebaseConfig = {
    apiKey: "AIzaSyBAnumrCwqbC6dNBqU5_FMZT6Z9S4wir9o",
    authDomain: "sayur-online-22fa9.firebaseapp.com",
    databaseURL: "https://sayur-online-22fa9.firebaseio.com",
    projectId: "sayur-online-22fa9",
    storageBucket: "sayur-online-22fa9.appspot.com",
    messagingSenderId: "132059841999",
    appId: "1:132059841999:web:c663eda21327d991d3beba",
    measurementId: "G-WVZYELDJYJ"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
//   firebase.analytics();

export const database = firebase.database(); 

export default firebase;

import firebase,{database} from "../../firebase";

export const actionUserName = () => (dispatch) => {
    setTimeout (() => {
        return dispatch({type: 'CHANGE_USER', value:'Anton Kristiono'});
    }, 2000);
};

export const registerUserAPI = (data) => (dispatch) => {
    return new Promise ((resolve,reject) => {
	dispatch({type:'CHANGE_LOADING', value: true});
	firebase
            .auth()
            .createUserWithEmailAndPassword(data.email, data.password)
            .then(res => {
                console.log("berhasil submit", res);
		dispatch({type:'CHANGE_LOADING', value: false});
		resolve(true);
            })
            .catch(function (error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode, errorMessage);
		dispatch({type:'CHANGE_LOADING', value: false});
		reject(false);
            });
    });
    
};

export const loginUserAPI = (data) => (dispatch) => {
    return new Promise ((resolve, reject) => {
	dispatch({type:'CHANGE_LOADING', value: true});
	firebase
            .auth()
            .signInWithEmailAndPassword(data.email, data.password)
            .then(res => {
                // console.log("berhasil submit", res);
                const dataUser = {
                    email:res.user.email,
                    uid:res.user.uid,
                    emailVerified:res.user.emailVerified,
		    refreshToken: res.user.refreshToken,
                };
		dispatch({type:'CHANGE_LOADING', value: false});
                dispatch({type:"CHANGE_USER", value: dataUser});
                dispatch({type:"CHANGE_ISLOGIN", value: true});
		resolve(dataUser);
            })
            .catch(function (error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode, errorMessage);
		dispatch({type:'CHANGE_LOADING', value: false});
		reject(false);
            });	
    });    
};

export const addBarangToAPI = (data) => (dispatch) => {
    database.ref('barangs/' + data.userId).push({
	namaBarang: data.namaBarang,
	hargaBeli: data.hargaBeli,
	jumlah: data.jumlah
    });
};

export const getDataFromAPI = (userId) => (dispatch) => {
    const urlBarangs = database.ref("barangs/"+ userId);
    return new Promise((resolve,reject) => {
	urlBarangs.on("value",function(snapshot){	    
	    // console.log("GET data:", snapshot.val());
	    const data = [];
	    // Object.keys di sini berfungsi untuk mengubah 
	    Object.keys(snapshot.val()).map( key => {
		data.push({
		    id:key,
		    data: snapshot.val()[key]
		});
	    });
	    dispatch({type: "SET_BARANGS", value: data});
	    resolve(snapshot.val());
	});
    });    
};

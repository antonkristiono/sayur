import React, {Component} from 'react';
import {Segment, Form, Input, Button } from 'semantic-ui-react';
import TombolButton from "../../../components/atoms/Button";
import {connect} from "react-redux";
import {addBarangToAPI} from "../../../config/redux/action";

class FormInputBarang extends Component {
    state = {
        namaBarang: "",
        hargaBeli: "",
        jumlah: ""
    }

    // componentDidMount () {
    //     const userData =  localStorage.getItem("userData");
    //     console.log("dashboard :", JSON.parse(userData));
    // }


    handleSaveBarang = () => {
        const {namaBarang, hargaBeli, jumlah}= this.state;
        const {saveBarang} =this.props;
	const userData = JSON.parse(localStorage.getItem("userData"));
	
        const data = {
            namaBarang: namaBarang,
            hargaBeli: hargaBeli,
            jumlah: jumlah,
            userId: userData.uid
        };        
        saveBarang(data);
        console.log(data);
    }

    onInputChange = (e,type) => {
        this.setState ({
            [type]: e.target.value 
        });
    }
    
    render() {
        const {namaBarang, hargaBeli, jumlah} = this.state;
        return (
            <Form>
              <Segment>
                <Form.Field inline>
                  <label>Nama Barang</label>
                  <Input
                    placeholder='Nama barang'
                    value={namaBarang}
                    onChange={(e)=> this.onInputChange(e,"namaBarang")}
                  />
                </Form.Field>
                <Form.Field inline>
                  <label>Harga</label>
                  <Input
                    placeholder='Harga Beli'
                    value={hargaBeli}
                    onChange={(e)=> this.onInputChange(e,"hargaBeli")}
                  />
                </Form.Field>
                <Form.Field inline>
                  <label>Jumlah Barang</label>
                  <Input
                    placeholder='Jumlah'
                    value={jumlah}
                    onChange={(e)=> this.onInputChange(e,"jumlah")}
                  />
                </Form.Field>
                <Form.Field style={{ width:"150px" }}>
                  <TombolButton onClick={this.handleSaveBarang}   title="submit" floated="left"/>
                </Form.Field>
              </Segment>
            </Form>
        );
    }
};

const reduxState = (state) => ({
    userData: state.user
});


const reduxDispatch = (dispatch) => ({
    saveBarang: (data) => dispatch(addBarangToAPI(data))
});

export default connect(reduxState,reduxDispatch) (FormInputBarang);

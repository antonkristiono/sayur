import React, {Component,Fragment} from "react";
import {getDataFromAPI} from "../../../config/redux/action";
import { connect } from "react-redux";
import {Table,Button,Segment, Grid,Card} from "semantic-ui-react";


class Transaksi extends Component {

    componentDidMount (){
        const userData = JSON.parse(localStorage.getItem("userData"));
        this.props.getBarangs(userData.uid);
    }

    handleClickPilih = (e) => {
        console.log(e);
    }
    
    render() {
        const {barangs} = this.props;
        console.log("barangss: ", barangs);
        return (
            <div>
              <Grid columns={2} divided>
                <Grid.Row stretched>
                  <Grid.Column width={5}>
                    <Segment>
                      <Table size="small">
                        
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell width={2}>Nama Barang</Table.HeaderCell>
                            <Table.HeaderCell width={2}>Harga</Table.HeaderCell>
                            <Table.HeaderCell width={2}>Jumlah</Table.HeaderCell>
                            <Table.HeaderCell width={2}>Total</Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>

                        <Table.Body>
                          <Table.Row>
                            <Table.Cell>John</Table.Cell>
                            <Table.Cell>Approved</Table.Cell>
                          </Table.Row>
                          <Table.Row>
                            <Table.Cell>Jamie</Table.Cell>
                            <Table.Cell>Approved</Table.Cell>
                          </Table.Row>
                          <Table.Row>
                            <Table.Cell>Jill</Table.Cell>
                            <Table.Cell>Denied</Table.Cell>
                          </Table.Row>
                        </Table.Body>

                        <Table.Footer>
                          <Table.Row>
                            <Table.HeaderCell></Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                            <Table.HeaderCell>3</Table.HeaderCell>
                            <Table.HeaderCell>200000</Table.HeaderCell>
                          </Table.Row>
                        </Table.Footer>
                      </Table>
                    </Segment>
                  </Grid.Column>
                  
                  <Grid.Column width={11}>
                    <Segment>
                      <div>
                        <Card.Group itemsPerRow={5}>
                          {
                              barangs.length > 0 ? (
                                  <Fragment>
                                    {
                                        barangs.map (barang => {
                                            return (
                                                <div key={barang.id}>
                                                  <Button onClick={this.handleClickPilih}>
                                                    <Card >
                                                      <p>{barang.data.namaBarang}</p>
                                                      <p>{barang.data.hargaBeli}</p>
                                                    </Card>
                                                  </Button>
                                                </div>
                                            );
                                        })
                                    }
                                  </Fragment>
                              ) : null
                          }
                        </Card.Group>
                      </div>
                    </Segment>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
        );
    }
}


const reduxState = (state) => ({
    barangs: state.barangs
});

const reduxDispatch = (dispatch) => ({
    getBarangs: (data) => dispatch(getDataFromAPI(data))
});

export default connect(reduxState,reduxDispatch)(Transaksi);

import React, { Component } from 'react';
import { Button, Menu, Segment,Icon } from 'semantic-ui-react';

export default class TopMenu extends Component {
    state = { activeItem: 'home' }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    render() {
        const { activeItem } = this.state;

        return (
            <Segment inverted>
              <Menu inverted pointing secondary>
                <Icon link name='content' />
                {/* <Menu.Item */}
                {/*   name='home' */}
                {/*   active={activeItem === 'home'} */}
                {/*   onClick={this.handleItemClick} */}
                {/* /> */}
                {/* <Menu.Item */}
                {/*   name='messages' */}
                {/*   active={activeItem === 'messages'} */}
                {/*   onClick={this.handleItemClick} */}
                {/* /> */}
                {/* <Menu.Item */}
                {/*   name='friends' */}
                {/*   active={activeItem === 'friends'} */}
                {/*   onClick={this.handleItemClick} */}
                {/* /> */}
              </Menu>
            </Segment>
        );
    }
}

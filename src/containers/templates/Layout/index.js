import React, { Component } from "react";
import {Header,
        Menu,
        Container,
        Image,
        Dropdown,
        Grid,
        Segment,
       } from "semantic-ui-react";
import TopMenu from "../../../containers/organisms/TopMenu";
import SideMenu from "../../../containers/organisms/SideMenu";
// import FormInputBarang from "../../organisms/FormInputBarang";
import Transaksi from "../../organisms/Transaksi";

class Dashboard extends Component {
    render() {
        return (
            <div>
              <TopMenu />
              <Grid>
                <Grid.Column width={2}>
                  <SideMenu />
                </Grid.Column>
                <Grid.Column width={12}>
                  <Transaksi />
                </Grid.Column>
              </Grid>
            </div>
        );
    }
}

export default Dashboard;

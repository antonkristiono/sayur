import React, { Component } from "react";
import {
    Button,
    Form,
    Grid,
    Header,
    Image,
    Message,
    Segment,
} from "semantic-ui-react";
import { connect} from "react-redux";
import {actionUserName} from "../../../config/redux/action";
import TombolButton from "../../../components/atoms/Button";
import LogoSayur from "../../../assets/img/logo/stand.png";
import { loginUserAPI } from "../../../config/redux/action";
import {withRouter} from "react-router-dom";

class Login extends Component {
    state = {
        email: "",
        password: "",
    };

    handleCangeText = (e) => {
        // console.log(e.target.id)
        this.setState({
            [e.target.id]: e.target.value,
        });
    };

    handleLoginSubmit = async () => {
        const { email, password } = this.state;
        const {history} = this.props;
	const res = await this.props.loginAPI({email,password}).catch(err => err);
        if(res){
            console.log("login berhasil",res);
            localStorage.setItem('userData',JSON.stringify(res));
            this.setState({
                email:"",
                password:""                
            });
            history.push('/');
            
        } else {
            console.log("login gagal");
        }
    };
    
    render() {
        return (
            <div>
              <Grid
                textAlign="center"
                style={{ height: "100vh" }}
                verticalAlign="middle"
              >
                <Grid.Column style={{ maxWidth: 450 }}>
                  <Header as="h2" color="teal" textAlign="center">
                    <Image src={LogoSayur} /> Log-in to your account {this.props.userName}
                  </Header>
                  <Form size="large">
                    <Segment stacked>
                      <Form.Input
                        fluid
                        icon="user"
                        iconPosition="left"
                        placeholder="E-mail address"
                        onChange={this.handleCangeText}
                        id="email"
                        value={this.state.email}
                      />
                      <Form.Input
                        fluid
                        icon="lock"
                        iconPosition="left"
                        placeholder="Password"
                        type="password"
                        onChange={this.handleCangeText}
                        id="password"
                        value={this.state.password}
                      />

                      <TombolButton onClick={this.handleLoginSubmit} title="Login" loading={this.props.isLoading}/>
                    </Segment>
                  </Form>
                  {/* <Message>
                     New to us? <a href="#">Sign Up</a>
                     </Message> */}
                </Grid.Column>
              </Grid>
            </div>
        );
    }
}


const reduxState = (state) => ({
    isLoading : state.isLoading
});

const reduxDispatch = (dispatch) => ({
    loginAPI: (data) => dispatch(loginUserAPI(data))
});


export default withRouter(connect(reduxState, reduxDispatch)(Login));

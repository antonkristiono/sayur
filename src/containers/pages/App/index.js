import React from "react";
import { BrowserRouter as Router, Switch, Route, Link  } from "react-router-dom";
import "./App.css";
import Dashboard from "../../../containers/pages/Dashboard";
import Login from "../Login";
import Register from "../Register";

import { Provider } from "react-redux";
import {store} from "../../../config/redux";


function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/">
            <Dashboard />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;

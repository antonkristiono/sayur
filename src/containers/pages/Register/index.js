import React, { Component } from "react";
import { Button, Form, Grid, Header, Image, Segment } from "semantic-ui-react";
import firebase from "../../../config/firebase";
import TombolButton from "../../../components/atoms/Button";
import LogoSayur from "../../../assets/img/logo/stand.png";
import {connect} from "react-redux";
import {registerUserAPI} from "../../../config/redux/action";

class Register extends Component {
    state = {
        email: "",
        password: "",
    };

    handleCangeText = (e) => {
        // console.log(e.target.id)
        this.setState({
            [e.target.id]: e.target.value,
        });
    };

    handleRegisterSubmit = async () => {
        const { email, password } = this.state;
	const res = await this.props.registerAPI({email,password}).catch(err => err);
	if (res) {
	    this.setState({
		email:"",
		password:""
            });
	} 
    };

    render() {
        return (
            <div>
              <Grid
                textAlign="center"
                style={{ height: "100vh" }}
                verticalAlign="middle"
              >
                <Grid.Column style={{ maxWidth: 450 }}>
                  <Header as="h2" color="teal" textAlign="center">
                    <Image src={LogoSayur} /> Silahkan Daftarkan E-mail Anda
                  </Header>
                  <Form size="large">
                    <Segment stacked>
                      <Form.Input
                        fluid
                        icon="user"
                        iconPosition="left"
                        placeholder="E-mail address"
                        onChange={this.handleCangeText}
                        id="email"
                        value={this.state.email}
                      />
                      <Form.Input
                        fluid
                        icon="lock"
                        iconPosition="left"
                        placeholder="Password"
                        type="password"
                        onChange={this.handleCangeText}
                        id="password"
                        value={this.state.password}
                      />
                      <TombolButton onClick={this.handleRegisterSubmit} title="Register" loading={this.props.isLoading}/>
                    </Segment>
                  </Form>
                </Grid.Column>
              </Grid>
            </div>
        );
    }
}

const reduxState = (state) => ({
    isLoading : state.isLoading
});

const reduxDispatch = (dispatch) => ({
    registerAPI: (data) => dispatch(registerUserAPI(data))
});

export default connect(reduxState,reduxDispatch)(Register);

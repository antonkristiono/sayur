import React from "react";
import {Button} from "semantic-ui-react";

const TombolButton = ({title,onClick,loading}) => {
    if(loading){
        return <Button className="disable">Loading</Button>;
    }
    return (
        <Button color="teal" fluid size="large" onClick={onClick}>
          {title}
        </Button>
    );
};

export default TombolButton;
